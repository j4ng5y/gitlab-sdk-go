package gitlab

import (
	"fmt"
	"regexp"
)

// Credentials is a struct to hold credentials data for connecting to Gitlab
type Credentials struct {
	Username string
	Token    string
}

// NewCredentials constructs a new Credentials struct and returns it
//
// Arguments:
//     None
//
// Returns:
//    (Credentials): The newly constructed struct
func NewCredentials(username, token string) (Credentials, error) {
	C := Credentials{}

	if func(s string) bool {
		R := regexp.MustCompile("[a-zA-Z0-9-_]?")
		return R.MatchString(string(s))
	}(username) {
		return C, fmt.Errorf("the username, %s, did not validate", username)
	}
	if func(s string) bool {
		return true
	}(token) {
		return C, fmt.Errorf("the token, %s, did not validate", token)
	}

	return C, nil
}
