package gitlab

import (
	"fmt"
	"net/http"
	"net/url"
	"strings"
)

// APIVersion is a string that can be validated to be a valid Gitlab API Version
type APIVersion string

// Validate will validate that a provided APIVersion type is valid
//
// Arguments:
//     None
//
// Returns:
//     ok (bool):   Whether or not the APIVersion in question is valid
//     err (error): An error if one exists, nil otherwise
func (A APIVersion) Validate() (ok bool, err error) {
	switch strings.ToLower(string(A)) {
	case "v4":
		return true, nil
	case "graphql":
		return true, nil
	default:
		return false, fmt.Errorf("%s is not a valid Gitlab API version", A)
	}
}

// Client is the primary client used to interface with Gitlab
type Client struct {
	APIVersion  APIVersion
	URL         *url.URL
	HTTPClient  *http.Client
	Credentials Credentials
}

// New constructs a new Client struct and returns it or an error
//
// Arguments:
//     gitlabURL (string): The base URL of your gitlab instance to query
//
// Returns:
//     (Client): The newly constructed Client struct or a blank one if an error occurred
//     (error):  A
func New(gitlabURL string) (Client, error) {
	var C Client

	// Validate that the URL provided is a valid one
	u, err := url.Parse(gitlabURL)
	if err != nil {
		return C, fmt.Errorf("the url provided, '%s', is not a valid URL, err: %w", gitlabURL, err)
	}

	C.URL = u
	C.HTTPClient = http.DefaultClient

	return C, nil
}

// SetAPIVersion sets the Gitlab API Version after validation
//
// Arguments:
//     v (APIVersion): An instance of the APIVersion type
//
// Returns:
//     (error): An error if one exists, nil otherwise
func (C *Client) SetAPIVersion(v APIVersion) error {
	if _, err := v.Validate(); err != nil {
		return err
	}
	C.APIVersion = v
	return nil
}

// SetCredentials sets the credentials to be used by the Client
//
// Arguments:
//     credentials (*credentials.Credentials): The credentials struct to use
//
// Returns:
//     None
func (C *Client) SetCredentials(credentials Credentials) {
	C.Credentials = credentials
}
