package gitlab

import "testing"

func TestGetAllGitignoreTemplates(t *testing.T) {
	C, err := New(gitlabURL)
	if err != nil {
		t.Fail()
	}

	_, err = C.GetAllGitignoreTemplates()
	if err != nil {
		t.Log(err)
		t.Fail()
	}
}

func TestGetGitignoreTemplate(t *testing.T) {
	C, err := New(gitlabURL)
	if err != nil {
		t.Fail()
	}

	_, err = C.GetGitignoreTemplate("Go")
	if err != nil {
		t.Log(err)
		t.Fail()
	}
}
