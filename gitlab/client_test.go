package gitlab

import "testing"

var gitlabURL string = "https://gitlab.com"
var crapURL string = "?!@#$%.#$%"

func TestNew(t *testing.T) {
	C, err := New(gitlabURL)
	if err != nil {
		t.Fail()
	}

	_, err = New(crapURL)
	if err == nil {
		t.Fail()
	}

	if C.URL.String() != gitlabURL {
		t.Fail()
	}

	if C.HTTPClient == nil {
		t.Fail()
	}
}

func TestSetCredentials(t *testing.T) {
	C, err := New(gitlabURL)
	if err != nil {
		t.Fail()
	}

	emptyCreds := Credentials{}
	creds := Credentials{
		Username: "j4ng5y",
		Token:    "12345",
	}
	C.SetCredentials(creds)

	if C.Credentials == emptyCreds {
		t.Fail()
	}
}
