package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"
)

// GetAllGitignoreTemplateResponses is the response object for fetching all gitignore template requests
type GetAllGitignoreTemplateResponses []struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetAllGitignoreTemplateResponses object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetAllGitignoreTemplateResponses) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetGitignoreTemplateResponse is the response object for single gitignore template reqquests
type GetGitignoreTemplateResponse struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetGitignoreTemplateResponse object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetGitignoreTemplateResponse) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetAllGitlabCIYMLTemplateResponses is the primary response object for gitignore template requests
type GetAllGitlabCIYMLTemplateResponses []struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetAllGitignoreTemplateResponses object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetAllGitlabCIYMLTemplateResponses) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetGitlabCIYMLTemplateResponse is the response object for single gitlab-ci.yml template reqquests
type GetGitlabCIYMLTemplateResponse struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetGitignoreTemplateResponse object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetGitlabCIYMLTemplateResponse) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetAllLicenseTemplateResponses is the primary response object for license template requests
type GetAllLicenseTemplateResponses []GetLicenseTemplateResponse

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetGitignoreTemplateResponse object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetAllLicenseTemplateResponses) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetLicenseTemplateRequest is the primary request object to license template requests
type GetLicenseTemplateRequest struct {
	Key      string `json:"key"`
	Project  string `json:"project"`
	FullName string `json:"fullname"`
}

// GetLicenseTemplateResponse is the primary response object for license template requests
type GetLicenseTemplateResponse struct {
	Key         string   `json:"key"`
	Name        string   `json:"name"`
	Nickname    string   `json:"nickname"`
	Featured    bool     `json:"featured"`
	HTMLURL     string   `json:"html_url"`
	SourceURL   string   `json:"source_url"`
	Description string   `json:"description"`
	Conditions  []string `json:"conditions"`
	Permissions []string `json:"permissions"`
	Limitations []string `json:"limitations"`
	Content     string   `json:"content"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetGitignoreTemplateResponse object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetLicenseTemplateResponse) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetAllDockerfileTemplateResponses is the primary response object for gitignore template requests
type GetAllDockerfileTemplateResponses []struct {
	Key  string `json:"key"`
	Name string `json:"name"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetAllGitignoreTemplateResponses object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetAllDockerfileTemplateResponses) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetDockerfileTemplateResponse is the response object for single dockerfile template reqquests
type GetDockerfileTemplateResponse struct {
	Name    string `json:"name"`
	Content string `json:"content"`
}

// Unmarshal is a light wrapper around json.Unmarshal that incorporates the reading of the html body as well.
//
// Unmarshalling is a process that puts the contents of a json response into a data object, in this case, the GetAllGitignoreTemplateResponses object.
//
// Arguments:
//    htmlBody (io.ReadCloser): The html body returned from an http client request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (G *GetDockerfileTemplateResponse) Unmarshal(htmlBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(htmlBody)
	if err != nil {
		return fmt.Errorf("unable to read the html body provided due to error, err: %w", err)
	}

	if err := json.Unmarshal(b, G); err != nil {
		return fmt.Errorf("unable to unmarshal the json response due to error, err: %w", err)
	}

	return nil
}

// GetAllGitignoreTemplates fetches all Gitignore templates and returns them in the GetAllGitignoreTemplatesResponses struct
//
// Arguments:
//     None
//
// Returns:
//     (*GetAllGitignoreTemplateResponses): A pointer to the GetAllGitignoreTemplateResponses object, or nil if an error occurred
//     (error):                             An error if one exists, nil otherwise
func (C Client) GetAllGitignoreTemplates() (*GetAllGitignoreTemplateResponses, error) {
	R := new(GetAllGitignoreTemplateResponses)

	fullURL := fmt.Sprintf("%s/api/v4/templates/gitignores", C.URL.String())

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetGitignoreTemplate fetches a Gitignore templates and returns them in the GetGitignoreTemplatesResponses struct
//
// Arguments:
//     key (string):  The key of the template to return
//
// Returns:
//     (*GetGitignoreTemplateResponse): A pointer to the GetGitignoreTemplateResponse object, or nil if an error occurred
//     (error):                         An error if one exists, nil otherwise
func (C Client) GetGitignoreTemplate(key string) (*GetGitignoreTemplateResponse, error) {
	R := new(GetGitignoreTemplateResponse)
	fullURL := fmt.Sprintf("%s/api/v4/templates/gitignores/%s", C.URL.String(), key)

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetAllGitlabCIYMLTemplates fetches all gitlab-ci templates and returns them in the GetAllGitlabCIYMLTemplatesResponses struct
//
// Arguments:
//     None
//
// Returns:
//     (*GetAllGitlabCIYMLTemplateResponses): A pointer to the GetAllGitlabCIYMLTemplateResponses object, or nil if an error occurred
//     (error):                               An error if one exists, nil otherwise
func (C Client) GetAllGitlabCIYMLTemplates() (*GetAllGitlabCIYMLTemplateResponses, error) {
	R := new(GetAllGitlabCIYMLTemplateResponses)

	fullURL := fmt.Sprintf("%s/api/v4/templates/gitlab_ci_ymls", C.URL.String())

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetGitlabCIYMLTemplate fetches a gitlab-ci template and returns them in the GetGitlabCIYMLTemplatesResponses struct
//
// Arguments:
//     key (string):  The key of the template to return
//
// Returns:
//     (*GetGitlabCIYMLTemplateResponse): A pointer to the GetGitlabCIYMLTemplateResponse object, or nil if an error occurred
//     (error):                           An error if one exists, nil otherwise
func (C Client) GetGitlabCIYMLTemplate(key string) (*GetGitlabCIYMLTemplateResponse, error) {
	R := new(GetGitlabCIYMLTemplateResponse)
	fullURL := fmt.Sprintf("%s/api/v4/templates/gitlab_ci_ymls/%s", C.URL.String(), key)

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetAllDockerfileTemplates fetches all dockerfile templates and returns them in the GetAllDockerfileTemplatesResponses struct
//
// Arguments:
//     None
//
// Returns:
//     (*GetAllDockerfileTemplateResponses): A pointer to the GetAllDockerfileTemplateResponses object, or nil if an error occurred
//     (error):                              An error if one exists, nil otherwise
func (C Client) GetAllDockerfileTemplates() (*GetAllDockerfileTemplateResponses, error) {
	R := new(GetAllDockerfileTemplateResponses)

	fullURL := fmt.Sprintf("%s/api/v4/templates/dockerfiles", C.URL.String())

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetDockerfileTemplate fetches a dockerfile template and returns them in the GetDockerfileTemplatesResponses struct
//
// Arguments:
//     key (string):  The key of the template to return
//
// Returns:
//     (*GetDockerfileTemplateResponse): A pointer to the GetDockerfileTemplateResponse object, or nil if an error occurred
//     (error):                          An error if one exists, nil otherwise
func (C Client) GetDockerfileTemplate(key string) (*GetDockerfileTemplateResponse, error) {
	R := new(GetDockerfileTemplateResponse)
	fullURL := fmt.Sprintf("%s/api/v4/templates/dockerfiles/%s", C.URL.String(), key)

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetAllLicenseTemplates fetches all license templates and returns them in the GetAllLicenseTemplatesResponses struct
//
// Arguments:
//     popular (bool): Only return popular licenses
//
// Returns:
//     (*GetAllLicenseTemplateResponses): A pointer to the GetAllLicenseTemplateResponses object, or nil if an error occurred
//     (error):                           An error if one exists, nil otherwise
func (C Client) GetAllLicenseTemplates(popular bool) (*GetAllLicenseTemplateResponses, error) {
	var fullURL string
	R := new(GetAllLicenseTemplateResponses)

	if popular {
		fullURL = fmt.Sprintf("%s/api/v4/templates/licenses", C.URL.String())
	} else {
		fullURL = fmt.Sprintf("%s/api/v4/templates/licenses?popular=1", C.URL.String())
	}

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// GetLicenseTemplate fetches a formatted license template and returns it in the GetLicenseTemplatesResponse struct
//
// Arguments:
//     request (GetLicenseTemplateRequest): A License Template request object
//
// Returns:
//     (*GetLicenseTemplateResponse): A pointer to the GetLicenseTemplateResponse object, or nil if an error occurred
//     (error):                       An error if one exists, nil otherwise
func (C Client) GetLicenseTemplate(request GetLicenseTemplateRequest) (*GetLicenseTemplateResponse, error) {
	var fullURL string
	R := new(GetLicenseTemplateResponse)

	switch request.Key {
	case "":
		return nil, fmt.Errorf("unable to process request, err: 'key' is requred")
	default:
		if request.Project != "" {
			if request.FullName != "" {
				n := strings.ReplaceAll(request.FullName, " ", "+")
				fullURL = fmt.Sprintf("%s/api/v4/templates/licenses/%s?project=%s&fullname=%s", C.URL.String(), request.Key, request.Project, n)
			} else {
				fullURL = fmt.Sprintf("%s/api/v4/templates/licenses/%s?project=%s", C.URL.String(), request.Key, request.Project)
			}
		} else {
			fullURL = fmt.Sprintf("%s/api/v4/templates/licenses/%s", C.URL.String(), request.Key)
		}
	}

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}
