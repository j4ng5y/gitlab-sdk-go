package gitlab

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

// AccessLevels are the human readable form of integer access levels assigned to users
const (
	AccessLevelNoAccess   = 0
	AccessLevelGuest      = 10
	AccessLevelReporter   = 20
	AccessLevelDeveloper  = 30
	AccessLevelMaintainer = 40
	AccessLevelOwner      = 50
)

// AccessRequestResponse is the struct that all of the responses are formed from
type AccessRequestResponse struct {
	ID          int       `json:"id"`
	Username    string    `json:"username"`
	Name        string    `json:"name"`
	State       string    `json:"state"`
	CreatedAt   time.Time `json:"created_at"`
	RequestedAt time.Time `json:"requested_at"`
	AccessLevel int       `json:"access_level,omitempty"`
}

// ListGroupAccessRequestsResponse is a list of AccessRequestResponses associated with a particular group
type ListGroupAccessRequestsResponse []AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *ListGroupAccessRequestsResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// ListProjectAccessRequestsResponse is a list of AccessRequestResponses associated with a particular project
type ListProjectAccessRequestsResponse []AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *ListProjectAccessRequestsResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// RequestGroupAccessResponse is an AccessRequestResponses associated with a particular group
type RequestGroupAccessResponse AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *RequestGroupAccessResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// RequestProjectAccessResponse is an AccessRequestResponses associated with a particular project
type RequestProjectAccessResponse AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *RequestProjectAccessResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// ApproveGroupAccessRequestResponse is an AccessRequestResponses associated with an access approval to particular group
type ApproveGroupAccessRequestResponse AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *ApproveGroupAccessRequestResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// ApproveProjectAccessRequestResponse is an AccessRequestResponses associated with an access approval to particular project
type ApproveProjectAccessRequestResponse AccessRequestResponse

// Unmarshal will translate an HTTP response body into a struct
//
// Arguements:
//     httpBody (io.ReadCloser): The http.Response.Body object returned from a request
//
// Returns:
//    (error): An error if one exists, nil otherwise
func (L *ApproveProjectAccessRequestResponse) Unmarshal(httpBody io.ReadCloser) error {
	b, err := ioutil.ReadAll(httpBody)
	if err != nil {
		return err
	}

	return json.Unmarshal(b, L)
}

// ListGroupAccessRequests gets a list of access requests viewable by the authenticated user
//
// Arguements:
//     groupID (int): The ID of the group to list access requests for
//
// Returns:
//     (*ListGroupAccessRequestsResponse): The post-json-marshalled response struct
//     (error):                            An error if one exists, nil otherwise
func (C Client) ListGroupAccessRequests(groupID int) (*ListGroupAccessRequestsResponse, error) {
	var fullURL string
	R := new(ListGroupAccessRequestsResponse)

	fullURL = fmt.Sprintf("%s/api/v4/groupss/%d/access_requests", C.URL.String(), groupID)

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// ListProjectAccessRequests gets a list of access requests viewable by the authenticated user
//
// Arguements:
//     projectID (int): The ID of the project to list access requests for
//
// Returns:
//     (*ListProjectAccessRequestsResponse): The post-json-marshalled response struct
//     (error):                              An error if one exists, nil otherwise
func (C Client) ListProjectAccessRequests(projectID int) (*ListProjectAccessRequestsResponse, error) {
	var fullURL string
	R := new(ListProjectAccessRequestsResponse)

	fullURL = fmt.Sprintf("%s/api/v4/projects/%d/access_requests", C.URL.String(), projectID)

	resp, err := C.HTTPClient.Get(fullURL)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a GET request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the GET call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// RequestGroupAccess requests access for the authenticated user to a group or project.
//
// Arguements:
//     groupID (int): The ID of the group to request access to
//
// Returns:
//     (*RequestGroupAccessRepsonse): The post-json-marshalled response struct
//     (error):                       An error if one exists, nil otherwise
func (C Client) RequestGroupAccess(groupID int) (*RequestGroupAccessResponse, error) {
	var fullURL string
	R := new(RequestGroupAccessResponse)

	fullURL = fmt.Sprintf("%s/api/v4/groups/%d/access_requests", C.URL.String(), groupID)

	resp, err := C.HTTPClient.Post(fullURL, "application/json", nil)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a POST request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the POST call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// RequestProjectAccess requests access for the authenticated user to a group or project.
//
// Arguements:
//     ProjectID (int): The ID of the project to request access to
//
// Returns:
//     (*RequestProjectAccessRepsonse): The post-json-marshalled response struct
//     (error):                         An error if one exists, nil otherwise
func (C Client) RequestProjectAccess(projectID int) (*RequestProjectAccessResponse, error) {
	var fullURL string
	R := new(RequestProjectAccessResponse)

	fullURL = fmt.Sprintf("%s/api/v4/projects/%d/access_requests", C.URL.String(), projectID)

	resp, err := C.HTTPClient.Post(fullURL, "application/json", nil)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a POST request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the POST call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// ApproveGroupAccessRequest approves an access request for the given user
//
// Arguements:
//     groupID (int): The ID of the group to approve access requests for
//     userID (int):  The ID of the user to approve access requests for
//
// Returns:
//     (*ApprovedGroupAccessRequestResponse): The post-json-marshalled response struct
//     (error):                               An error if one exists, nil otherwise
func (C Client) ApproveGroupAccessRequest(groupID, userID int) (*ApproveGroupAccessRequestResponse, error) {
	var fullURL string
	R := new(ApproveGroupAccessRequestResponse)

	fullURL = fmt.Sprintf("%s/api/v4/groups/%d/access_requests/%d/approve", C.URL.String(), groupID, userID)

	req, err := http.NewRequest(http.MethodPut, fullURL, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to generate HTTP Request to '%s' due error, err: %w", fullURL, err)
	}

	resp, err := C.HTTPClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a PUT request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the PUT call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// ApproveProjectAccessRequest approves an access request for the given user
//
// Arguements:
//     projectID (int): The ID of the group to approve access requests for
//     userID (int):    The ID of the user to approve access requests for
//
// Returns:
//     (*ApprovedProjectAccessRequestResponse): The post-json-marshalled response struct
//     (error):                                 An error if one exists, nil otherwise
func (C Client) ApproveProjectAccessRequest(projectID, userID int) (*ApproveProjectAccessRequestResponse, error) {
	var fullURL string
	R := new(ApproveProjectAccessRequestResponse)

	fullURL = fmt.Sprintf("%s/api/v4/projects/%d/access_requests/%d/approve", C.URL.String(), projectID, userID)

	req, err := http.NewRequest(http.MethodPut, fullURL, nil)
	if err != nil {
		return nil, fmt.Errorf("unable to generate HTTP Request to '%s' due error, err: %w", fullURL, err)
	}

	resp, err := C.HTTPClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("unable to perform a PUT request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("the PUT call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	if err := R.Unmarshal(resp.Body); err != nil {
		return nil, fmt.Errorf("unable to unmarshal the response due to error, err: %w", err)
	}

	return R, nil
}

// DenyGroupAccessRequest denies an access request for the given user
//
// Arguements:
//     groupID (int): The ID of the group to deny access to
//     userID (int):  The ID of the user to deny access to
//
// Returns:
//     (error):                            An error if one exists, nil otherwise
func (C Client) DenyGroupAccessRequest(groupID, userID int) error {
	var fullURL string

	fullURL = fmt.Sprintf("%s/api/v4/groups/%d/access_requests/%d", C.URL.String(), groupID, userID)

	req, err := http.NewRequest(http.MethodDelete, fullURL, nil)
	if err != nil {
		return fmt.Errorf("unable to generate HTTP Request to '%s' due error, err: %w", fullURL, err)
	}

	resp, err := C.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("unable to perform a DELETE request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("the DELETE call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	return nil
}

// DenyProjectAccessRequest denies an access request for the given user
//
// Arguements:
//     projectID (int): The ID of the group to deny access to
//     userID (int):    The ID of the user to deny access to
//
// Returns:
//     (error):                            An error if one exists, nil otherwise
func (C Client) DenyProjectAccessRequest(projectID, userID int) error {
	var fullURL string

	fullURL = fmt.Sprintf("%s/api/v4/projectss/%d/access_requests/%d", C.URL.String(), projectID, userID)

	req, err := http.NewRequest(http.MethodDelete, fullURL, nil)
	if err != nil {
		return fmt.Errorf("unable to generate HTTP Request to '%s' due error, err: %w", fullURL, err)
	}

	resp, err := C.HTTPClient.Do(req)
	if err != nil {
		return fmt.Errorf("unable to perform a DELETE request to '%s' due to error, err: %w", fullURL, err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("the DELETE call to '%s' returned a non-200 status of %d", fullURL, resp.StatusCode)
	}

	return nil
}
