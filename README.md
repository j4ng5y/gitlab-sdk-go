![logo][logo]

# Gitlab SDK for Go

This project is intended to be an all inclusive SDK for working with Gitlab for the Go language.

## Importing

`go get -u gitlab.com/j4ng5y/gitlab-sdk-go/gitlab`

## Examples

Examples can be found in the [Examples](./examples) folder.

## Feature Tracking

| Gitlab API Resource        | Gitlab API Version | SDK Supported?     |
|:--------------------------:|:------------------:|:------------------:|
| .gitignore (templates)     | v4                 | :white_check_mark: |
|                            | graphql            | :x:                |
| .gitlab-ci.yml (templates) | v4                 | :white_check_mark: |
|                            | graphql            | :x:                |
| Access Requests            | v4                 | :white_check_mark: |
|                            | graphql            | :x:                |
| Apperance (applications)   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Applications               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Avatar                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Award emoji                | v4                 | :x:                |
|                            | graphql            | :x:                |
| Badges (project)           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Badges (group)             | v4                 | :x:                |
|                            | graphql            | :x:                |
| Branches                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Broadcast Messages         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Clusters (project)         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Clusters (group)           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Commits                    | v4                 | :x:                |
|                            | graphql            | :x:                |
| Container Registry         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Custom Attributes          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Dependencies               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Deploy Keys                | v4                 | :x:                |
|                            | graphql            | :x:                |
| Deployments                | v4                 | :x:                |
|                            | graphql            | :x:                |
| Discussions                | v4                 | :x:                |
|                            | graphql            | :x:                |
| Dockerfile (templates)     | v4                 | :white_check_mark: |
|                            | graphql            | :x:                |
| Environments               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Epics                      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Events                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Feature Flags              | v4                 | :x:                |
|                            | graphql            | :x:                |
| Geo Nodes                  | v4                 | :x:                |
|                            | graphql            | :x:                |
| Groups                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Import                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Issue Boards (project)     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Issue Boards (group)       | v4                 | :x:                |
|                            | graphql            | :x:                |
| Issues                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Issues (epic)              | v4                 | :x:                |
|                            | graphql            | :x:                |
| Issues statistics          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Jobs                       | v4                 | :x:                |
|                            | graphql            | :x:                |
| Keys                       | v4                 | :x:                |
|                            | graphql            | :x:                |
| Labels (project)           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Labels (group)             | v4                 | :x:                |
|                            | graphql            | :x:                |
| License                    | v4                 | :x:                |
|                            | graphql            | :x:                |
| Licenses (templates)       | v4                 | :white_check_mark: |
|                            | graphql            | :x:                |
| Links (issue)              | v4                 | :x:                |
|                            | graphql            | :x:                |
| Links (epic)               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Managed Licenses           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Markdown                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Members                    | v4                 | :x:                |
|                            | graphql            | :x:                |
| Merge request approvals    | v4                 | :x:                |
|                            | graphql            | :x:                |
| Merge requests             | v4                 | :x:                |
|                            | graphql            | :x:                |
| Milestones (project)       | v4                 | :x:                |
|                            | graphql            | :x:                |
| Milestones (group)         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Namespaces                 | v4                 | :x:                |
|                            | graphql            | :x:                |
| Notes (comments)           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Notification settings      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Packages                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Pages domains              | v4                 | :x:                |
|                            | graphql            | :x:                |
| Pipelines schedules        | v4                 | :x:                |
|                            | graphql            | :x:                |
| Pipeline triggers          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Pipelines                  | v4                 | :x:                |
|                            | graphql            | :x:                |
| Project aliases            | v4                 | :x:                |
|                            | graphql            | :x:                |
| Project import/export      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Project statistics         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Project templates          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Projects                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Protected brances          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Protected tags             | v4                 | :x:                |
|                            | graphql            | :x:                |
| Releases                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Release links              | v4                 | :x:                |
|                            | graphql            | :x:                |
| Repositories               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Repository files           | v4                 | :x:                |
|                            | graphql            | :x:                |
| Repository submodules      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Resource lebel events      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Runners                    | v4                 | :x:                |
|                            | graphql            | :x:                |
| SCIM                       | v4                 | :x:                |
|                            | graphql            | :x:                |
| Search                     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Services                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Settings (application)     | v4                 | :x:                |
|                            | graphql            | :x:                |
| Sidekiq metrics            | v4                 | :x:                |
|                            | graphql            | :x:                |
| Snippets                   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Snippets (project)         | v4                 | :x:                |
|                            | graphql            | :x:                |
| Statistics (application)   | v4                 | :x:                |
|                            | graphql            | :x:                |
| Suggestions                | v4                 | :x:                |
|                            | graphql            | :x:                |
| System hooks               | v4                 | :x:                |
|                            | graphql            | :x:                |
| Tags                       | v4                 | :x:                |
|                            | graphql            | :x:                |
| To-Do lists                | v4                 | :x:                |
|                            | graphql            | :x:                |
| Users                      | v4                 | :x:                |
|                            | graphql            | :x:                |
| Variables (project)        | v4                 | :x:                |
|                            | graphql            | :x:                |
| Variables (group)          | v4                 | :x:                |
|                            | graphql            | :x:                |
| Version                    | v4                 | :x:                |
|                            | graphql            | :x:                |
| Vulnerabilities            | v4                 | :x:                |
|                            | graphql            | :x:                |
| Wikis                      | v4                 | :x:                |
|                            | graphql            | :x:                |

[logo]: ./img/gitlab-sdk-go.png "logo"