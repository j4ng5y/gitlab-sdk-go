package main

import (
	"fmt"
	"log"

	"gitlab.com/j4ng5y/gitlab-sdk-go.git"
)

func main() {
	c, err := gitlab.New("https://gitlab.com/")
	if err != nil {
		log.Fatal(err)
	}

	r, err := c.GetAllDockerfileTemplates()
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range *r {
		fmt.Printf("Key: %s, Name: %s\n", v.Key, v.Name)
	}
}
