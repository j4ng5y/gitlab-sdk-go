package main

import (
	"fmt"
	"log"

	"gitlab.com/j4ng5y/gitlab-sdk-go/gitlab"
)

func main() {
	c, err := gitlab.New("https://gitlab.com")
	if err != nil {
		log.Fatal(err)
	}

	req := gitlab.GetLicenseTemplateRequest{
		Key:      "mit",
		Project:  "gitlab-sdk-go",
		FullName: "Jordan Gregory",
	}

	r, err := c.GetLicenseTemplate(req)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf(
		licenseTemplate,
		r.Key,
		r.Name,
		r.Nickname,
		r.Featured,
		r.HTMLURL,
		r.SourceURL,
		r.Description,
		r.Conditions,
		r.Permissions,
		r.Limitations,
		r.Content)
}

const licenseTemplate string = `Key: %s
Name: %s
Nickname: %s
Featured: %v
HTML URL: %s
Source URL: %s
Description: %s
Conditions:
  %v
Permissions:
  %v
Limitations:
  %v
Content:
%s`
