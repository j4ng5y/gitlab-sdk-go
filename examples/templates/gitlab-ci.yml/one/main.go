package main

import (
	"fmt"
	"log"

	"gitlab.com/j4ng5y/gitlab-sdk-go/gitlab"
)

func main() {
	c, err := gitlab.New("https://gitlab.com/")
	if err != nil {
		log.Fatal(err)
	}

	r, err := c.GetGitlabCIYMLTemplate("Go")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(r.Content)
}
