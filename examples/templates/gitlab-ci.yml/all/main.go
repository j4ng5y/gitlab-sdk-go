package main

import (
	"fmt"
	"log"

	"gitlab.com/j4ng5y/gitlab-sdk-go/gitlab"
)

func main() {
	c, err := gitlab.New("https://gitlab.com/")
	if err != nil {
		log.Fatal(err)
	}

	r, err := c.GetAllGitlabCIYMLTemplates()
	if err != nil {
		log.Fatal(err)
	}

	for _, v := range *r {
		fmt.Printf("Key: %s, Name: %s\n", v.Key, v.Name)
	}
}
